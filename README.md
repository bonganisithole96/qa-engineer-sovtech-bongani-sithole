# README #

This project needs to run through Katalon studio.

### What is in this project ###

It consist of 1 test suite, 3 test cases (Contact us, Clients and Blog) and 1 Excel test data

-Navigate to the "Test Cases" folder to view all the test cases
-Navigate to the "Object Repository" and expand objects folder to view all the object repository
-Navigate to the "Data Files" folder to view where the test data is stored
-Navigate to the "Test Suites" folder to view the suite
-Navigate to the "Reports" folder to view al the reports

### Test Steps ###
-Launch the Test Suite which consists of 3 test cases(Contact us, Clients and Blog)
-Click the Run button
-Contact us will run first and it will get values from a test data which uses excel
-Clients will run second
-Blog will run last.
-Click on "Reports" folder and expand to see the test execution report
NB do you close the application while it is running, it will close on its won
